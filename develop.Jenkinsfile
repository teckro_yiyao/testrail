#!/usr/bin/env groovy

pipeline {

    agent {
         label 'small'
    }

    options {
        disableConcurrentBuilds()
        timestamps()
        timeout(time: 1, unit: 'HOURS')
    }

    tools {
        jdk 'jdk11-installer'
    }


    parameters {
        string(defaultValue: "email@teckro.com", description: 'testrail user email', name: 'email')
        string(defaultValue: "*****", description: 'testrail user encrypted psw', name: 'psw')
        choice(choices: ['search'], description: 'release project', name: 'project')
        string(defaultValue: "*****", description: 'release name. example: 2022.1.0', name: 'release')

        booleanParam(defaultValue: false, description: 'production bugs on search project', name: 'bugs')
    }

    stages  {
         stage('Checkout Project') {
             steps {
                 script {
                     currentBuild.displayName = "#${BUILD_NUMBER} [${project}][${release}]"
                     cleanWs()
                     checkout scm
                     withCredentials([file(credentialsId: 'jenkins-nexus-gradle', variable: 'GRADLE_CREDS')]) {
                         sh("cat \$GRADLE_CREDS > gradle.properties")
                     }
                 }
             }
         }
         stage('Run Tests') {
             steps {
                 script {
                     def gradleExecution = "./gradlew clean build releaseSetup -Demail=${email} -Dpsw=${psw} -Dproject=${project} -Drelease=${release} -Dbugs=${bugs}"
                     gradleExecution = gradleExecution + " -Dtestng.dtd.http=true --refresh-dependencies"
                     sh (gradleExecution)
                 }
             }
         }
    }

    post {
        always {
            script {
                archiveArtifacts artifacts: "artifacts/${namespace}_testData*", fingerprint: true
            }
        }
    }
}
