package com.teckro.client.testrail.deserializer.responses;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teckro.client.testrail.retrofit.payload.responses.PlanResponse;
import com.teckro.client.testrail.retrofit.payload.responses.PlansResponse;
import com.teckro.client.testrail.retrofit.payload.responses.ResultResponse;
import com.teckro.client.testrail.retrofit.payload.responses.RunsResponse;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class ResponseDeserializationTests {

    @SneakyThrows
    @Test
    public void deserializingGetPlansResponse() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/get_plans_response.json");
        PlansResponse plansResponse = mapper.readValue(url, PlansResponse.class);

        assertThat(plansResponse.getPlans().size(), is(1));
    }

    @SneakyThrows
    @Test
    public void deserializingGetRunsResponse() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/get_runs_response.json");
        RunsResponse runsResponse = mapper.readValue(url, RunsResponse.class);

        assertThat(runsResponse.getRuns().size(), is(2));
    }

    @SneakyThrows
    @Test
    public void deserializingGetPlanResponse() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/get_plan_response.json");
        PlanResponse planResponse = mapper.readValue(url, PlanResponse.class);

        assertThat(planResponse.getEntries().get(0).getRuns().size(), is(2));
    }

    @SneakyThrows
    @Test
    public void deserializingAddResultResponse() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/add_result_response.json");
        ResultResponse resultResponse = mapper.readValue(url, ResultResponse.class);

        assertThat(resultResponse.getStepResults().size(), is(2));
    }

    @SneakyThrows
    @Test
    public void deserializingAddResultNoStepsResponse() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/add_result_response_no_steps.json");

        ResultResponse resultResponse = mapper.readValue(url, ResultResponse.class);

        assertThat(resultResponse.getStepResults(), nullValue());

        System.out.println(Integer.valueOf(System.getProperty("runId", "-1")));
        System.out.println(System.getProperty("psw", "a"));
    }
}
