package com.teckro.client.testrail.deserializer.requests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teckro.client.testrail.retrofit.payload.requests.CaseResultRequest;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class RequestDeserializationTests {

    @SneakyThrows
    @Test
    public void deserializingCaseResultRequest() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/add_result_request.json");
        CaseResultRequest caseResultRequest = mapper.readValue(url, CaseResultRequest.class);

        assertThat(caseResultRequest.getStepsResults().size(), is(2));
    }

    @SneakyThrows
    @Test
    public void deserializingCaseResultRequestNoSteps() {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("file:src/test/resources/add_result_request_no_steps.json");
        CaseResultRequest caseResultRequest = mapper.readValue(url, CaseResultRequest.class);

        assertThat(caseResultRequest.getStepsResults(), nullValue());
    }
}
