package com.teckro.client.testrail.tests;

import com.teckro.client.testrail.TestBase;
import com.teckro.client.testrail.enums.ProjectIdEnum;
import com.teckro.client.testrail.enums.TrainingSuitesEnum;
import com.teckro.client.testrail.retrofit.payload.responses.MilestoneResponse;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.teckro.client.testrail.enums.SearchSuitesEnum.REGRESSION;
import static com.teckro.client.testrail.enums.SearchSuitesEnum.SMOKE;
import static com.teckro.client.testrail.enums.TrainingSuitesEnum.FEATURE;
import static com.teckro.client.testrail.helpers.MilestoneHelper.createMilestone;
import static com.teckro.client.testrail.helpers.MilestoneHelper.createSubMilestone;
import static com.teckro.client.testrail.helpers.MilestoneHelper.getMilestoneId;
import static com.teckro.client.testrail.helpers.TestRunsHelper.createTestRun;
import static com.teckro.client.testrail.utils.SystemVarUtil.BUGS;
import static com.teckro.client.testrail.utils.SystemVarUtil.PROJECT;
import static com.teckro.client.testrail.utils.SystemVarUtil.RELEASE_NAME;

public class ReleaseSetupTests extends TestBase {

    @SneakyThrows
    @Test
    public void CreateReleaseTest() {
        Integer projectId;
        List<MilestoneResponse> milestones;
        switch (PROJECT) {
            case ("search"):
                projectId = ProjectIdEnum.SEARCH.getProjectId();
                milestones = createSearchMilestones(projectId);
                createSearchTestRuns(projectId, milestones);
                break;
            case ("training"):
                projectId = ProjectIdEnum.TRAINING.getProjectId();
                milestones = createSearchMilestones(projectId);
                createTrainingTestRuns(projectId, milestones);
                break;
            default:
                throw new NoSuchFieldException(String.format("project [%s] does not exist", PROJECT));
        }
    }

    private List<MilestoneResponse> createSearchMilestones(Integer projectId) {
        List<MilestoneResponse> milestones = new ArrayList<>();

        String releaseName = String.format("LAUNCH%s Release", RELEASE_NAME);
        MilestoneResponse milestone = createMilestone(releaseName, projectId);
        milestones.add(milestone);
        Integer parentId = milestone.getId();

        if (BUGS) {
            String subName = String.format("%s - Production Bug Fixes", RELEASE_NAME);
            MilestoneResponse subMilestone = createSubMilestone(releaseName, subName, projectId, parentId);
            milestones.add(subMilestone);
        }

        String subName = String.format("%s - Search V2 Regression", RELEASE_NAME);
        MilestoneResponse subMilestone = createSubMilestone(releaseName, subName, projectId, parentId);
        milestones.add(subMilestone);

        subName = String.format("%s - Search V2 Validation", RELEASE_NAME);
        subMilestone = createSubMilestone(releaseName, subName, projectId, parentId);
        milestones.add(subMilestone);

        subName = String.format("%s - Production Smoke Test", RELEASE_NAME);
        subMilestone = createSubMilestone(releaseName, subName, projectId, parentId);
        milestones.add(subMilestone);

        return milestones;
    }

    private void createSearchTestRuns(Integer projectId, List<MilestoneResponse> milestones) {
        String runName = String.format("%s - Search V2 Automated Regression", RELEASE_NAME);
        String subName = String.format("%s - Search V2 Regression", RELEASE_NAME);
        Integer milestoneId = getMilestoneId(milestones, subName);
        createTestRun(runName, projectId, milestoneId, REGRESSION.getSuiteId(), null);

        runName = String.format("%s - Production Smoke Test", RELEASE_NAME);
        subName = String.format("%s - Production Smoke Test", RELEASE_NAME);
        milestoneId = getMilestoneId(milestones, subName);
        createTestRun(runName, projectId, milestoneId, SMOKE.getSuiteId(), List.of(11804, 20470, 22687));
    }

    private void createTrainingTestRuns(Integer projectId, List<MilestoneResponse> milestones) {
        String runName = String.format("%s - feature", RELEASE_NAME);
        String subName = String.format("%s - Search V2 Regression", RELEASE_NAME);
        Integer milestoneId = getMilestoneId(milestones, subName);
        createTestRun(runName, projectId, milestoneId, FEATURE.getSuiteId(), null);

        runName = String.format("%s - smoke", RELEASE_NAME);
        subName = String.format("%s - Production Smoke Test", RELEASE_NAME);
        milestoneId = getMilestoneId(milestones, subName);
        createTestRun(runName, projectId, milestoneId, TrainingSuitesEnum.SMOKE.getSuiteId(), List.of(11702, 3942));
    }
}
