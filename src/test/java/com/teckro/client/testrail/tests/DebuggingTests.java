package com.teckro.client.testrail.tests;

import com.teckro.client.testrail.TestBase;
import com.teckro.client.testrail.data.TestRailResultData;
import com.teckro.client.testrail.helpers.TestRailHelper;
import com.teckro.client.testrail.listners.TestRail;
import lombok.SneakyThrows;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls.addAttachmentToResult;

public class DebuggingTests extends TestBase {

    private List<TestRailResultData> results;
    private TestRailHelper testRailHelper;

    @BeforeClass
    public void testsSetup() {
        results = Collections.synchronizedList(new ArrayList<>());
    }

    @BeforeMethod
    public void testSetup(Method testMethod) {
        testRailHelper = new TestRailHelper(testMethod, results);
    }

    @SneakyThrows
    @Test(enabled = false)
    public void attach() {
        addAttachmentToResult(30510, "smile.png");
    }

    @TestRail(id = 10437, comments = "step1: as expected\nstep2: as expected\nstep3: as expected")
    @Test(description = "simple test case")
    public void runTest1() {
        //test does something
    }

    @SneakyThrows
    @TestRail(id = 10437, comments = "step1: as expected\nstep2: as expected\nstep3: as expected")
    @Test(description = "simple test case")
    public void runTest2() {
        throw new Exception("something went wrong");
        //test does something
    }

    @TestRail(id = 17605, steps = 4)
    @Test(description = "multistep test case no failure")
    public void runTest3() {
        System.out.println("1");
        testRailHelper.passTestStep(1);

        System.out.println("2");
        testRailHelper.passTestStep(2);

        System.out.println("3");
        testRailHelper.passTestStep(3);

        System.out.println("4");
        testRailHelper.passTestStep(4);
    }

    @SneakyThrows
    @TestRail(id = 17605, steps = 4)
    @Test(description = "multistep test case with failure")
    public void runTest4() {
        System.out.println("1");
        testRailHelper.passTestStep(1);

        System.out.println("2");
        testRailHelper.passTestStep(2);

        System.out.println("3");
        throw new Exception("step3 failed on purpose");
    }

    @SneakyThrows
    @AfterMethod(alwaysRun = true)
    public void testTeardown(ITestResult iTestResult, Method testMethod) {
        testRailHelper.processTestResult(testMethod, iTestResult);
    }

    @AfterClass(alwaysRun = true)
    public void testsTeardown() {
        testRailHelper.updateTestsStatus(results);
    }
}
