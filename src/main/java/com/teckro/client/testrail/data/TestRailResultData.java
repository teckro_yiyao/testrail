package com.teckro.client.testrail.data;

import com.teckro.client.testrail.enums.StatusEnum;
import com.teckro.client.testrail.retrofit.payload.requests.StepResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = PRIVATE)
public class TestRailResultData {
    Integer id;
    StatusEnum status;
    String comment;
    String assignedToId;
    List<StepResult> stepResults;
}
