package com.teckro.client.testrail.helpers;

import com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls;
import com.teckro.client.testrail.retrofit.payload.requests.MilestoneRequest;
import com.teckro.client.testrail.retrofit.payload.responses.MilestoneResponse;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import retrofit2.Response;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

import static com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls.getMilestones;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = PRIVATE)
public class MilestoneHelper {

    @SneakyThrows
    public static MilestoneResponse createMilestone(String milestoneName, Integer projectId) {

        MilestoneResponse milestone = findMilestone(milestoneName, projectId);

        if (milestone == null) {
            milestone = buildMilestone(milestoneName, projectId, null);
        }

        return milestone;
    }

    @SneakyThrows
    public static MilestoneResponse createSubMilestone(String milestoneName, String subMilestoneName, Integer projectId, Integer parentId) {

        MilestoneResponse milestone = findMilestone(milestoneName, projectId);

        if (milestone == null) {
            throw new NoSuchElementException(String.format("cannot create sub milestone. Parent milestone [%s] does not exist", milestoneName));
        }

        List<MilestoneResponse> subMilestones = Objects.requireNonNull(milestone.getSubMilestones());
        Optional<MilestoneResponse> existingSubMilestone = subMilestones.stream()
                .filter(sub -> sub.getName().equalsIgnoreCase(subMilestoneName))
                .findAny();

        return existingSubMilestone.orElseGet(() -> buildMilestone(subMilestoneName, projectId, parentId));
    }


    private static MilestoneResponse findMilestone(String milestoneName, Integer projectId) {
        List<MilestoneResponse> milestones = Objects.requireNonNull(getMilestones(projectId, false).body()).getMilestones();
        Optional<MilestoneResponse> existingMilestone = milestones.stream()
                .filter(milestoneResponse -> milestoneResponse.getName().equalsIgnoreCase(milestoneName))
                .findAny();

        return existingMilestone.orElse(null);
    }

    private static MilestoneResponse buildMilestone(String name, Integer projectId, Integer parentId) {
        MilestoneRequest request = MilestoneRequest.builder()
                .name(name)
                .build();

        if (parentId != null) {
            request.setParentId(parentId);
        }

        Response<MilestoneResponse> response = TestRailAPICalls.createMilestone(projectId, request);
        assertThat("response code mismatched", response.code(), is(SC_OK));

        return Objects.requireNonNull(response.body());
    }

    public static Integer getMilestoneId(List<MilestoneResponse> milestones, String milestoneName) {
        MilestoneResponse found = milestones.stream()
                .filter(milestoneResponse -> milestoneResponse.getName().equalsIgnoreCase(milestoneName))
                .findAny()
                .orElseThrow(() -> new NoSuchElementException(String.format("milestone with name [%s] not found on the list", milestoneName)));
        return found.getId();
    }
}
