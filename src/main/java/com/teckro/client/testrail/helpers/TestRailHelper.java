package com.teckro.client.testrail.helpers;

import com.teckro.client.testrail.data.TestRailResultData;
import com.teckro.client.testrail.listners.TestRail;
import com.teckro.client.testrail.retrofit.payload.requests.CaseResultRequest;
import com.teckro.client.testrail.retrofit.payload.requests.StepResult;
import com.teckro.client.testrail.retrofit.payload.responses.TestsResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.ITestResult;
import retrofit2.Response;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.teckro.client.testrail.enums.StatusEnum.FAILED;
import static com.teckro.client.testrail.enums.StatusEnum.PASSED;
import static com.teckro.client.testrail.enums.StatusEnum.UNTESTED;
import static com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls.addResultsForCase;
import static com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls.getTests;
import static com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls.getUser;
import static com.teckro.client.testrail.retrofit.payload.requests.StepResult.stepBuilder;
import static com.teckro.client.testrail.utils.SystemVarUtil.RUN_ID;
import static com.teckro.client.testrail.utils.SystemVarUtil.USER_EMAIL;
import static java.util.Objects.requireNonNull;

@Slf4j
public class TestRailHelper {

    private TestRailResultData result;
    private List<StepResult> stepResults;

    public TestRailHelper(Method testMethod, List<TestRailResultData> results) {
        if (testMethod.isAnnotationPresent(TestRail.class)) {
            int numberOfSteps = testMethod.getAnnotation(TestRail.class).steps();
            result = TestRailResultData.builder().build();
            results.add(result);

            if (numberOfSteps != 0) {
                stepResults = new ArrayList<>();
                result.setStepResults(stepResults);

                for (int i = 0; i < numberOfSteps; i++) {
                    StepResult stepResult = stepBuilder();
                    stepResults.add(stepResult);
                }
            }
        }
    }

    public void passTestStep(int stepNumber) {
        stepResults.get(stepNumber - 1).setContent("step worked as expected");
        stepResults.get(stepNumber - 1).setStatusId(PASSED.getStatusId());
    }

    public void processTestResult(Method testMethod, ITestResult iTestResult) {
        if (testMethod.isAnnotationPresent(TestRail.class)) {
            TestRail testRail = testMethod.getAnnotation(TestRail.class);

            result.setId(testRail.id());
            switch (iTestResult.getStatus()) {
                case ITestResult.SUCCESS:
                    result.setStatus(PASSED);
                    result.setComment(testRail.comments());
                    break;
                case ITestResult.FAILURE:
                    result.setStatus(FAILED);

                    if (result.getStepResults() == null) {
                        result.setComment("Test failed: " + iTestResult.getThrowable().getMessage());
                    } else {
                        StepResult failedStep = result.getStepResults().stream()
                                .filter(stepResult -> stepResult.getStatusId().intValue() != PASSED.getStatusId().intValue())
                                .findFirst()
                                .orElseThrow(() -> new NoSuchElementException("Something wrong: all steps has status passed."));
                        failedStep.setContent("Step failed: " + iTestResult.getThrowable().getMessage());
                        failedStep.setStatusId(FAILED.getStatusId());
                    }
                    break;
                default:
                    result.setStatus(UNTESTED);
            }
        }
    }

    @SneakyThrows
    public void updateTestsStatus(List<TestRailResultData> results) {
        Response<TestsResponse> response = getTests(RUN_ID);

        if (response.code() == 200) {

            String assignedToId = requireNonNull(getUser(USER_EMAIL).body()).getId().toString();

            results.forEach(testRailResult -> uploadTestStatusToTestRail(assignedToId, testRailResult));
        } else {
            log.warn(requireNonNull(response.errorBody()).string());
        }
    }

    public void uploadTestStatusToTestRail(String assignedToId, TestRailResultData result) {
        CaseResultRequest request = CaseResultRequest.builder()
                .statusId(result.getStatus().getStatusId())
                .comment(result.getComment())
                .assignedToId(assignedToId)
                .build();

        if (result.getStepResults() != null) {
            request.setStepsResults(result.getStepResults());
        }

        addResultsForCase(RUN_ID, result.getId(), request);
    }
}
