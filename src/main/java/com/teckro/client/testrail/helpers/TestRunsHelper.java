package com.teckro.client.testrail.helpers;

import com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls;
import com.teckro.client.testrail.retrofit.payload.requests.RunRequest;
import com.teckro.client.testrail.retrofit.payload.responses.RunResponse;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import retrofit2.Response;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.teckro.client.testrail.retrofit.endpoints.TestRailAPICalls.getTestRuns;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = PRIVATE)
public class TestRunsHelper {

    @SneakyThrows
    public static RunResponse createTestRun(String runName, Integer projectId, Integer milestoneId, Integer suiteId, List<Integer> cases) {

        RunResponse run = findTestRun(runName, projectId);

        if (run == null) {
            run = buildTestRun(runName, projectId, milestoneId, suiteId, cases);
        }

        return run;
    }

    private static RunResponse findTestRun(String runName, Integer projectId) {
        List<RunResponse> runs = Objects.requireNonNull(getTestRuns(projectId).body()).getRuns();
        Optional<RunResponse> existingRun = runs.stream()
                .filter(run -> run.getName().equalsIgnoreCase(runName))
                .findAny();

        return existingRun.orElse(null);
    }

    private static RunResponse buildTestRun(String runName, Integer projectId, Integer milestoneId, Integer suiteId, List<Integer> cases) {
        RunRequest request = RunRequest.builder()
                .suiteId(suiteId)
                .name(runName)
                .milestoneId(milestoneId)
                .includeAll(true)
                .build();

        if (cases != null) {
            request.setIncludeAll(false);
            request.setCaseIds(cases);
        }

        Response<RunResponse> response = TestRailAPICalls.createRun(projectId, request);
        assertThat("response code mismatched", response.code(), is(SC_OK));

        return Objects.requireNonNull(response.body());
    }
}
