package com.teckro.client.testrail.utils;

import lombok.NoArgsConstructor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class EncryptionUtil {

    public static String decryptWord(String encryptedText) {
        String[] s = encryptedText.split("\\.", 2);
        int iterations = Integer.parseInt(s[0]);
        return getEncryptor(iterations).decrypt(s[1]);
    }

    public static StandardPBEStringEncryptor getEncryptor(Integer iterations) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("Teckro01!");
        encryptor.setKeyObtentionIterations(iterations);

        return encryptor;
    }
}