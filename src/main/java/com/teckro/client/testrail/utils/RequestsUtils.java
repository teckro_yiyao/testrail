package com.teckro.client.testrail.utils;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import java.io.File;
import java.util.Base64;

import static com.teckro.client.testrail.utils.EncryptionUtil.decryptWord;
import static com.teckro.client.testrail.utils.SystemVarUtil.USER_EMAIL;
import static com.teckro.client.testrail.utils.SystemVarUtil.USER_PSW;
import static java.nio.charset.StandardCharsets.UTF_8;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class RequestsUtils {
    public static String basiAuthConverter() {
        byte[] auth = (USER_EMAIL + ":" + decryptWord(USER_PSW)).getBytes(UTF_8);
        byte[] encodedAuth = Base64.getEncoder().encode(auth);
        return "Basic " + new String(encodedAuth);
    }

    @SneakyThrows
    public static MultipartBody.Part convertFileToPart(String imageName) {
        File file = new File("src/test/resources/" + imageName);
        RequestBody requestBody = RequestBody.create(file, MediaType.parse("image/*"));

        return MultipartBody.Part.createFormData("attachment", file.getName(), requestBody);
    }

}
