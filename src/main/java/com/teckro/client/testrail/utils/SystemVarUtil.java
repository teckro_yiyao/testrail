package com.teckro.client.testrail.utils;

import lombok.NoArgsConstructor;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.parseBoolean;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class SystemVarUtil {

    public static final String USER_EMAIL = System.getProperty("email");
    public static final String USER_PSW = System.getProperty("psw");
    public static final String RELEASE_NAME = System.getProperty("release");

    public static final Integer RUN_ID = Integer.valueOf(System.getProperty("runId", "-1"));
    public static final String PROJECT = System.getProperty("project");
    public static final boolean BUGS = parseBoolean(System.getProperty("bugs", FALSE.toString()));
}
