package com.teckro.client.testrail.retrofit.endpoints;

import com.teckro.client.testrail.retrofit.RetrofitBuilder;
import com.teckro.client.testrail.retrofit.payload.requests.CaseResultRequest;
import com.teckro.client.testrail.retrofit.payload.requests.MilestoneRequest;
import com.teckro.client.testrail.retrofit.payload.requests.RunRequest;
import com.teckro.client.testrail.retrofit.payload.responses.AttachmentResponse;
import com.teckro.client.testrail.retrofit.payload.responses.MilestoneResponse;
import com.teckro.client.testrail.retrofit.payload.responses.MilestonesResponse;
import com.teckro.client.testrail.retrofit.payload.responses.PlanResponse;
import com.teckro.client.testrail.retrofit.payload.responses.PlansResponse;
import com.teckro.client.testrail.retrofit.payload.responses.ResultResponse;
import com.teckro.client.testrail.retrofit.payload.responses.RunResponse;
import com.teckro.client.testrail.retrofit.payload.responses.RunsResponse;
import com.teckro.client.testrail.retrofit.payload.responses.TestsResponse;
import com.teckro.client.testrail.retrofit.payload.responses.UserResponse;
import com.teckro.client.testrail.utils.RequestsUtils;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import okhttp3.MultipartBody;
import retrofit2.Response;

import static lombok.AccessLevel.PRIVATE;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;

@NoArgsConstructor(access = PRIVATE)
public class TestRailAPICalls {

    private static final TestRailAPIEndpoints CALL = new RetrofitBuilder().getRetrofit().create(TestRailAPIEndpoints.class);

    @SneakyThrows
    public static Response<RunsResponse> getTestRuns(Integer projectId) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.getRuns(projectId, auth).execute();
    }

    @SneakyThrows
    public static Response<RunResponse> createRun(Integer projectId, RunRequest requestBody) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.createRun(projectId, requestBody, auth).execute();
    }

    @SneakyThrows
    public static Response<PlansResponse> getTestPlans(Integer projectId) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.getPlans(projectId, auth).execute();
    }

    @SneakyThrows
    public static Response<PlanResponse> getTestPlan(Integer planId) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.getPlan(planId, auth).execute();
    }

    public static Response<TestsResponse> getTests(Integer runId) {
        return getTests(runId, 0, 250, "1,2,3,4,5");
    }

    @SneakyThrows
    public static Response<TestsResponse> getTests(Integer runId, Integer offset, Integer limit, String statusIds) {

        String auth = RequestsUtils.basiAuthConverter();
        return CALL.getTests(runId, limit, offset, statusIds, auth).execute();
    }

    @SneakyThrows
    public static Response<UserResponse> getUser(String email) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.getUser(email, auth).execute();
    }

    @SneakyThrows
    public static Response<ResultResponse> addResultsForCase(Integer runId, Integer caseId, CaseResultRequest requestBody) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.addResultsForCase(runId, caseId, requestBody, APPLICATION_JSON.getMimeType(), auth).execute();
    }

    @SneakyThrows
    public static Response<AttachmentResponse> addAttachmentToResult(Integer resultId, String imageName) {
        String auth = RequestsUtils.basiAuthConverter();
        MultipartBody.Part filePart = RequestsUtils.convertFileToPart(imageName);
        return CALL.addAttachmentToResult(resultId, filePart, auth).execute();
    }

    @SneakyThrows
    public static Response<MilestonesResponse> getMilestones(Integer projectId, boolean isCompleted) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.getMilestones(projectId, isCompleted ? 1 : 0, auth).execute();
    }

    @SneakyThrows
    public static Response<MilestoneResponse> createMilestone(Integer projectId, MilestoneRequest requestBody) {
        String auth = RequestsUtils.basiAuthConverter();
        return CALL.createMilestone(projectId, requestBody, auth).execute();
    }
}
