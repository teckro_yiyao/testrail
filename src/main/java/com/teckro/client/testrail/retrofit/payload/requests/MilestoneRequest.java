package com.teckro.client.testrail.retrofit.payload.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MilestoneRequest {

    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;
    @JsonProperty("duo_on")
    Long duoOn;
    @JsonProperty("parent_id")
    Integer parentId;
    @JsonProperty("refs")
    String refs;
    @JsonProperty("start_on")
    String startOn;
}
