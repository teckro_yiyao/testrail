package com.teckro.client.testrail.retrofit.payload.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RunRequest {

    @JsonProperty("suite_id")
    Integer suiteId;
    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;
    @JsonProperty("milestone_id")
    Integer milestoneId;
    @JsonProperty("assignedto_id")
    Integer assignedtoId;
    @JsonProperty("include_all")
    Boolean includeAll;
    @JsonProperty("case_ids")
    List<Integer> caseIds;
    @JsonProperty("refs")
    String refs;
}
