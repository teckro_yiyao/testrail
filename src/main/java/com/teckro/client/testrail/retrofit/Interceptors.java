package com.teckro.client.testrail.retrofit;

import lombok.NoArgsConstructor;
import okhttp3.Interceptor;
import okhttp3.Request;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class Interceptors {

    public static Interceptor testRailUrlInterceptor() {
        return chain -> {
            Request request = chain.request();
            String finalURL = request.url().toString().replace("index.php/", "index.php?/");
            Request newReq = request.newBuilder()
                    .url(finalURL)
                    .build();
            return chain.proceed(newReq);
        };
    }
}
