package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MilestoneResponse {

    @JsonProperty("completed_on")
    Long completedOn;
    @JsonProperty("description")
    String description;
    @JsonProperty("due_on")
    Long dueOn;
    @JsonProperty("id")
    Integer id;
    @JsonProperty("is_completed")
    Boolean isCompleted;
    @JsonProperty("is_started")
    Boolean isStarted;
    @JsonProperty("milestones")
    List<MilestoneResponse> subMilestones;
    @JsonProperty("name")
    String name;
    @JsonProperty("parent_id")
    Integer parentId;
    @JsonProperty("project_id")
    Integer projectId;
    @JsonProperty("refs")
    String refs;
    @JsonProperty("start_on")
    Long startOn;
    @JsonProperty("started_on")
    Long startedOn;
    @JsonProperty("url")
    String url;
}
