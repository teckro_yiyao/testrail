package com.teckro.client.testrail.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;

import static com.teckro.client.testrail.retrofit.Interceptors.testRailUrlInterceptor;

public class RetrofitBuilder {

    private static final String BASE_URL = System.getProperty("base_url", "https://teckro.testrail.io/index.php/api/v2/");
    private static final Integer TIMEOUT = Integer.valueOf(System.getProperty("timeout", "60"));
    private final StringBuilder logMessage = new StringBuilder();
    private final Retrofit retrofit;

    public RetrofitBuilder() {
        retrofit = buildRetrofit(buildClient());
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    private OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(new HttpLoggingInterceptor(message -> logMessage.append(message).append("\n"))
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(testRailUrlInterceptor())
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    private Retrofit buildRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client)
                .build();
    }
}
