package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Plan {
    @JsonProperty("id")
    Integer id;
    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;
    @JsonProperty("milestone_id")
    Integer milestoneId;
    @JsonProperty("assignedto_id")
    Integer assignedToId;
    @JsonProperty("is_completed")
    Boolean isCompleted;
    @JsonProperty("completed_on")
    Integer completedOn;
    @JsonProperty("passed_count")
    Integer passedCount;
    @JsonProperty("blocked_count")
    Integer blockedCount;
    @JsonProperty("untested_count")
    Integer untestedCount;
    @JsonProperty("retest_count")
    Integer retestCount;
    @JsonProperty("failed_count")
    Integer failedCount;
    @JsonProperty("custom_status1_count")
    Integer customStatus1Count;
    @JsonProperty("custom_status2_count")
    Integer customStatus2Count;
    @JsonProperty("custom_status3_count")
    Integer customStatus3Count;
    @JsonProperty("custom_status4_count")
    Integer customStatus4Count;
    @JsonProperty("custom_status5_count")
    Integer customStatus5Count;
    @JsonProperty("custom_status6_count")
    Integer customStatus6Count;
    @JsonProperty("custom_status7_count")
    Integer customStatus7Count;
    @JsonProperty("project_id")
    Integer projectId;
    @JsonProperty("created_on")
    Integer createdOn;
    @JsonProperty("refs")
    String refs;
    @JsonProperty("created_by")
    Integer createdBy;
    @JsonProperty("url")
    String url;
}
