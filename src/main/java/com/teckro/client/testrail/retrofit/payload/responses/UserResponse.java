package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    @JsonProperty("name")
    String name;
    @JsonProperty("id")
    Integer id;
    @JsonProperty("email")
    String email;
    @JsonProperty("is_active")
    Boolean isActive;
    @JsonProperty("role_id")
    Integer roleId;
    @JsonProperty("role")
    String role;
}
