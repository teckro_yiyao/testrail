package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MilestonesResponse {

    @JsonProperty("offset")
    Integer offset;
    @JsonProperty("limit")
    Integer limit;
    @JsonProperty("size")
    Integer size;
    @JsonProperty("_links")
    Links links;
    @JsonProperty("milestones")
    List<MilestoneResponse> milestones;
}
