package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Entry {
    @JsonProperty("id")
    String id;
    @JsonProperty("suite_id")
    Integer suiteId;
    @JsonProperty("name")
    String name;
    @JsonProperty("refs")
    String refs;
    @JsonProperty("description")
    String description;
    @JsonProperty("include_all")
    Boolean includeAll;
    @JsonProperty("runs")
    List<RunResponse> runs;
}
