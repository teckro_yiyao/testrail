package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Step {
    @JsonProperty("content")
    String content;
    @JsonProperty("expected")
    String expected;
    @JsonProperty("actual")
    String actual;
    @JsonProperty("additional_info")
    String additionalInfo;
    @JsonProperty("refs")
    String refs;
    @JsonProperty("status_id")
    String statusId;
}
