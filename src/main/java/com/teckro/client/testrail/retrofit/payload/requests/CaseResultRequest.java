package com.teckro.client.testrail.retrofit.payload.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CaseResultRequest {

    @JsonProperty("status_id")
    Integer statusId;
    @JsonProperty("comment")
    String comment;
    @JsonProperty("elapsed")
    String elapsed;
    @JsonProperty("defects")
    String defects;
    @JsonProperty("version")
    String version;
    @JsonProperty("assignedto_id")
    String assignedToId;

    @JsonProperty("custom_step_results")
    List<StepResult> stepsResults;
}
