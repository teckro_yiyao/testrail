package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResultResponse {
    @JsonProperty("id")
    Integer id;
    @JsonProperty("test_id")
    Integer testId;
    @JsonProperty("status_id")
    Integer statusId;
    @JsonProperty("created_on")
    Integer createdOn;
    @JsonProperty("assignedto_id")
    Integer assignedtoId;
    @JsonProperty("comment")
    String comment;
    @JsonProperty("version")
    String version;
    @JsonProperty("elapsed")
    String elapsed;
    @JsonProperty("defects")
    String defects;
    @JsonProperty("created_by")
    Integer createdBy;
    @JsonProperty("custom_step_results")
    List<Step> stepResults;
    @JsonProperty("attachment_ids")
    List<String> attachmentIds;
}
