package com.teckro.client.testrail.retrofit.payload.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.teckro.client.testrail.enums.StatusEnum.UNTESTED;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StepResult {
    @JsonProperty("content")
    String content;
    @JsonProperty("expected")
    String expected;
    @JsonProperty("actual")
    String actual;
    @JsonProperty("status_id")
    Integer statusId;

    public static StepResult stepBuilder() {
        return StepResult.builder()
                .content("step not executed")
                .statusId(UNTESTED.getStatusId())
                .build();
    }
}
