package com.teckro.client.testrail.retrofit.endpoints;

import com.teckro.client.testrail.retrofit.payload.requests.CaseResultRequest;
import com.teckro.client.testrail.retrofit.payload.requests.MilestoneRequest;
import com.teckro.client.testrail.retrofit.payload.requests.RunRequest;
import com.teckro.client.testrail.retrofit.payload.responses.AttachmentResponse;
import com.teckro.client.testrail.retrofit.payload.responses.MilestoneResponse;
import com.teckro.client.testrail.retrofit.payload.responses.MilestonesResponse;
import com.teckro.client.testrail.retrofit.payload.responses.PlanResponse;
import com.teckro.client.testrail.retrofit.payload.responses.PlansResponse;
import com.teckro.client.testrail.retrofit.payload.responses.ResultResponse;
import com.teckro.client.testrail.retrofit.payload.responses.RunResponse;
import com.teckro.client.testrail.retrofit.payload.responses.RunsResponse;
import com.teckro.client.testrail.retrofit.payload.responses.TestsResponse;
import com.teckro.client.testrail.retrofit.payload.responses.UserResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpHeaders.CONTENT_TYPE;

public interface TestRailAPIEndpoints {

    String PROJECT_ID = "projectId";
    String RUN_ID = "runId";
    String CASE_ID = "caseId";

    @GET("get_runs/{" + PROJECT_ID + "}")
    Call<RunsResponse> getRuns(@Path(PROJECT_ID) Integer projectId, @Header(AUTHORIZATION) String auth);

    @POST("add_run/{" + PROJECT_ID + "}")
    Call<RunResponse> createRun(@Path(PROJECT_ID) Integer projectId,
                                @Body RunRequest requestBody,
                                @Header(AUTHORIZATION) String auth);

    @GET("get_plans/{" + PROJECT_ID + "}")
    Call<PlansResponse> getPlans(@Path(PROJECT_ID) Integer projectId, @Header(AUTHORIZATION) String auth);

    @GET("get_plan/{planId}")
    Call<PlanResponse> getPlan(@Path("planId") Integer planId, @Header(AUTHORIZATION) String auth);

    @GET("get_tests/{runId}&limit={limit}&offset={offset}&status_id={statusIds}")
    Call<TestsResponse> getTests(@Path("runId") Integer runId,
                                 @Path("limit") Integer limit,
                                 @Path("offset") Integer offset,
                                 @Path("statusIds") String statusIds,
                                 @Header(AUTHORIZATION) String auth);

    @POST("add_result_for_case/{" + RUN_ID + "}/{" + CASE_ID + "}")
    Call<ResultResponse> addResultsForCase(@Path(RUN_ID) Integer runId,
                                           @Path(CASE_ID) Integer caseId,
                                           @Body CaseResultRequest requestBody,
                                           @Header(CONTENT_TYPE) String contentType,
                                           @Header(AUTHORIZATION) String auth);

    @Multipart
    @POST("add_attachment_to_result/{resultId}")
    Call<AttachmentResponse> addAttachmentToResult(@Path("resultId") Integer resultId,
                                                   @Part MultipartBody.Part filePart,
                                                   @Header(AUTHORIZATION) String auth);


    @GET("get_user_by_email&email={email}")
    Call<UserResponse> getUser(@Path("email") String email, @Header(AUTHORIZATION) String auth);


    @GET("get_milestones/{" + PROJECT_ID + "}&is_completed={isCompleted}")
    Call<MilestonesResponse> getMilestones(@Path(PROJECT_ID) Integer projectId,
                                           @Path("isCompleted") Integer isCompleted,
                                           @Header(AUTHORIZATION) String auth);

    @POST("add_milestone/{" + PROJECT_ID + "}")
    Call<MilestoneResponse> createMilestone(@Path(PROJECT_ID) Integer projectId,
                                            @Body MilestoneRequest requestBody,
                                            @Header(AUTHORIZATION) String auth);
}
