package com.teckro.client.testrail.retrofit.payload.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Test {
    @JsonProperty("id")
    Integer id;
    @JsonProperty("case_id")
    Integer caseId;
    @JsonProperty("status_id")
    Integer statusId;
    @JsonProperty("title")
    String title;
    @JsonProperty("run_id")
    Integer runId;
    @JsonProperty("assignedto_id")
    Integer assignedToId;
    @JsonProperty("template_id")
    Integer templateId;
    @JsonProperty("type_id")
    Integer typeId;
    @JsonProperty("priority_id")
    Integer priorityId;
    @JsonProperty("estimate")
    String estimate;
    @JsonProperty("estimate_forecast")
    String estimateForecast;
    @JsonProperty("refs")
    String refs;
    @JsonProperty("milestone_id")
    String milestoneId;

    @JsonProperty("custom_urs")
    String urs;
    @JsonProperty("custom_tm")
    String tm;
    @JsonProperty("custom_tag")
    String tag;
    @JsonProperty("custom_is_automated")
    Boolean isAutomated;
    @JsonProperty("custom_confluence")
    String confluence;
    @JsonProperty("custom_preconds")
    String preconds;
    @JsonProperty("custom_steps")
    String steps;
    @JsonProperty("custom_steps_separated")
    List<Step> stepsSeparated;
    @JsonProperty("custom_mission")
    String mission;
    @JsonProperty("custom_sbe_example")
    String sbeExample;
    @JsonProperty("custom_expected")
    String expected;

    @JsonProperty("sections_display_order")
    Integer sectionsDisplayOrder;
    @JsonProperty("cases_display_order")
    Integer casesDisplayOrder;
}
