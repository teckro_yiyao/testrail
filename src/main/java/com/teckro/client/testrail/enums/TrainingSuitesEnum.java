package com.teckro.client.testrail.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TrainingSuitesEnum {
    FEATURE(406),
    SMOKE(245),
    IMPORT(1270);

    private final Integer suiteId;
}
