package com.teckro.client.testrail.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SearchSuitesEnum {
    CM_3_1(420),
    END_USER_3_2(419),
    SOA_3_3(418),
    EXTRACTION_3_4(417),
    ANALYTICS_3_5(435),
    BUGS(431),
    SMOKE(438),
    REGRESSION(430);

    private final Integer suiteId;
}
