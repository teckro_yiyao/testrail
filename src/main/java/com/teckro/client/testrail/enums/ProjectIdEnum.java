package com.teckro.client.testrail.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProjectIdEnum {
    SEARCH(24),
    ENGAGE(33),
    CONNECT(25),
    PATIENT(32),
    SHARED_SERVICES(28),
    STUDY_MANAGEMENT(27),
    TRAINING(21);

    private final Integer projectId;
}
